import java.io.*;

/**
* Jarko Papalitsas 501250
*
* Käyttöliittymä virtuaalikoneen käyttämiseen
*
*/

public class BootHelper {
	public static void main(String[] args) {
		if (args.length != 1) { // Onko jotain argumenttia annettu
			System.out.println("Anna muistivedos argumenttina");
			System.exit(1);
		}
		Tools.showDebugMessages(true); // Näytetäänkö viestit koneen tapahtumista
		try {
			Processor testProcessor = new Processor(args[0]); // Luodaan uusi prosessori, jonka keskusmuistiin ladataan tiedosto
			testProcessor.doCycle(); // Aloitetaan prosessorisykli
		}

		catch (IOException e) { // Jos muistitiedoston lukuvaiheessa on ongelmia...
			System.out.println("Virhe luettaessa muistitiedostoa"); // ...anna virheilmoitus...
			System.exit(1); // ...ja poistu virhekoodilla
		}

		System.exit(0); // Jos kaikki ok, postutaan kiltisti
	}

}
