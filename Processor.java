import java.io.*;

/**
* Jarko Papalitsas 501250
*
* Prosessoriluokka. Matkii yksinkertaista prosessoria.
* Käskyrekisteri: 16 bit, muut rekisterit: 8 bit. Rekistereiden osoitteet: 4 bit.
* Komennot jaettu 4:ään bittiin
*
*
*/

public class Processor {

	private byte[] register; // Prosessorin rekisteri
	private Memory memory; // Liitetty muisti
	private Storage storage; // Liitetty massamuisti
	private byte[] instructionRegister; // Käskyrekisteri
	private ProgramCounter programCounter; // Ohjelmalaskuri
	private boolean doCycle; // Ilmoittaa, lähteekö konesykli uudelle kierrokselle (HALT muuttaa)

	/**
	*
	* Prosessorin alustusmetodi
	* Määrittelee oletusena rekisterin 16-paikkaiseksi (0-F)
	*
	*/

	public Processor(String memoryFile) throws IOException {
		this.doCycle = true;
		this.register = new byte[16];
		this.memory = new Memory(memoryFile);
		this.storage = new Storage();
		this.instructionRegister = new byte[2];
		this.programCounter = new ProgramCounter(new byte[2]); // Sovitaan aloituskohdaksi ensimmäinen muistipaikka LISÄÄ TÄHÄN VIELÄ MUISTIN KOKO -ARGUMENTTI

	}

	// Tästä alkaa prosessorin konesykliin vaadittavat funktiot: Nouto - Purku - Suoritus, sekä itse syklimetodi

	/*
	* Syklifunktio sisältää kaikki oikeaan sykliin vaadittavat funktiot 
	* oikeassa jarjestyksessä
	*
	*
	*/

	public void doCycle() {
		while (this.doCycle) { // Tehdaan ikuisesti (tai kunnes kutsutaan HALT (eli break))
			fetch();
			run(parse(instructionRegister));
		
		}

	}

	/**
	* Noutaa muistista seuraavat muistiblokit, kunnes kaskyrekisteri on taytetty.
	* Ohjelmalaskuri päivittaa itsensä itsenäisesti. Toimii siis lukuvirran tavoin
	*
	*/

	private void fetch() {
		for (int i=0; this.instructionRegister.length>i; i=i+1) { // Taytetaan kaskyrekisteri
			this.instructionRegister[i] = memory.readBlock(programCounter.read());
		}

	}

	/**
	* Erottaa raa'asta bittijonosta komennon ja operandit toisistaan
	* 
	*
	*/

	private byte[] parse(byte[] rawBytes) {
		byte[] parts = new byte[rawBytes.length*2]; // Tehdään taulukko, joka on 2x suurempi 
		for (int i=0; rawBytes.length>i; i=i+1) { // Koska jokainen konekasky jakautuu 4-bittisiin palasiin, parsitaan kaskyrekisteri sellaisiksi.
			parts[2*i] = (byte) ((rawBytes[i] & 0xff) >>> 4); // Siirretaan bitteja, jotta erotetaan operandit ja komennot toisistaan. Maskataan ANDilla ykkösiin, jotta looginen siirto onnistuu byteinä
			parts[2*i+1] = (byte) (rawBytes[i] << 4);
			parts[2*i+1] = (byte) ((parts[2*i+1] & 0xff) >>> 4);
		}

		return parts;

	}

	/**
	* Suorittaa taulukossa saamansa käskyn, sekä välittää samaisessa taulukossa olleet operandit oikealle funktiolle
	* @.pre operands.length() == 3
	*
	*/

	private void run(byte[] commands) {
		byte[] operands = new byte[commands.length-1];
		for (int i=1; commands.length>i; i=i+1) { // Siirrä komennoista operandit omaan taulukkoonsa
			operands[i-1] = commands[i];
		}
		Tools.debugMessage("Suoritetaan komento: " + commands[0]);
		switch (commands[0]) {
			case 0: halt(operands); break;
			case 1: load(operands); break;
			case 2: loadByte(operands); break;
			case 3: store(operands); break;
			case 4: move(operands); break;
			case 5: add(operands); break;
			case 6: or(operands); break;
			case 7: and(operands); break;
			case 8: xor(operands); break;
			case 9: rotate(operands); break;
			case 10: jump(operands); break;
			case 11: readStorage(operands); break;
			case 12: saveStorage(operands); break;
			default: halt(operands); break; // Jos tuntematon komento, pysäytä ohjelma
		
		}

		

	}

	/*
	* Rekisterin tallennuskomento
	*
	* @.pre address < this.register.length()
	*/

	private void saveRegister(byte address, byte data) {
		Tools.debugMessage("Tallennetaan rekisteriin " + address + " arvo " + data);
		this.register[address] = data;

	}

	/*
	* Rekisterin lukukomento
	* 
	* @.pre address < this.register.length()
	*/

	private byte readRegister(byte address) {
		Tools.debugMessage("Luetaan rekisteriä " + address + ", arvo: " + this.register[address]);
		return this.register[address];

	}

	// Tästä alkaa konekielen funktiot

	/**
	* Lataa rekisteriin operands[0] muistipaikan operands[1] ja operands[2] sisallon
	* @.pre operands.length() == 3
	*
	*/

	private void load(byte[] operands) {
		byte[] address = {operands[1],operands[2]};
		this.saveRegister(operands[0], this.memory.readBlock(address));

	}

	/**
	* Lataa rekisteriin operands[0] operands[1] ja operands[2] sisallon
	* @.pre operands.length() == 3
	*
	*/

	private void loadByte(byte[] operands) {
		byte co = (byte)(operands[1] + (byte)(operands[2] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		this.saveRegister(operands[0], co); 

	}

	/**
	* Tallettaa rekisteriin operands[0] operands[1] ja operands[2] sisallon
	* @.pre operands.length() == 3
	*
	*/

	private void store(byte[] operands) {
		byte[] address = {operands[1],operands[2]};
		this.memory.storeBlock(address,this.readRegister(operands[0]));

	}

	/**
	* Kopioi rekisterin operands[1] sisällön rekisteriin operands[2]
	* @.pre operands.length() == 3
	*
	*/

	private void move(byte[] operands) {
		this.saveRegister(operands[2], this.readRegister(operands[1]));
	}

	/**
	* Yhteenlaskee rekisteripaikkojen operands[1] ja operands[2] sisällön yhteen ja sijoittaa sen rekisteripaikkaan operands[0]
	* @.pre operands.length() == 3
	*
	*/

	private void add(byte[] operands) {
		this.saveRegister(operands[0], (byte)(this.register[operands[1]] + this.register[operands[2]]));

	}

	/**
	* Tekee TAI-bittivertailun rekisteripaikan operands[1] ja operands[2] välille ja sijoittaa tuloksen rekisteripaikkaan opernads[0]
	* @.pre operands.length() == 3
	*
	*/

	private void or(byte[] operands) {
		this.saveRegister(operands[0],(byte)(this.readRegister(operands[1]) | this.readRegister(operands[2])));

	}

	/**
	* Tekee JA-bittivertailun rekisteripaikan operands[1] ja operands[2] välille ja sijoittaa tuloksen rekisteripaikkaan opernads[0]
	* @.pre operands.length() == 3
	*
	*/

	private void and(byte[] operands) {
		this.saveRegister(operands[0],(byte)(this.readRegister(operands[1]) & this.readRegister(operands[2])));

	}

	/**
	* Tekee XOR-bittivertailun (jos molemmat erit) rekisteripaikan operands[1] ja operands[2] välille ja sijoittaa tuloksen rekisteripaikkaan opernads[0]
	* @.pre operands.length() == 3
	*
	*/

	private void xor(byte[] operands) {
		this.saveRegister(operands[0],(byte)(this.readRegister(operands[1]) ^ this.readRegister(operands[2])));

	}

	/**
	* Siirtää operands[0] kohdassa olevan rekisterin bittiarvoa opernads[2] verran oikealle ja lisää ylimenevät vasemmalle
	* @.pre operands.length() == 3
	*
	*/

	private void rotate(byte[] operands) {
		this.saveRegister(operands[0], (byte)(this.readRegister(operands[0]) >> operands[2]));

	}

	/**
	* Ehdollinen hyppy. Jos rekisterissä 0 oleva luku on sama kuin rekisterissä operands[0], niin muutetaan ohjelmalaskurin arvoa operands[1] oprands[2]:ksi
	* @.pre operands.length() == 3
	*
	*/

	private void jump(byte[] operands) {
		byte[] address = {operands[1],operands[2]}; // TARKISTA JÄRJESTYS
		if (this.readRegister((byte)0) == this.readRegister(operands[0])) {
			this.programCounter.setPosition(address);

		}


	}

	/**
	* Pysäyttää ohjelman suorituksen
	*
	*/

	private void halt(byte[] operands) {
		this.storage.sync(); // Kirjoitetaan muutokset massamuistilaitteelle
		this.doCycle = false; // Pysäytetään konesykli

	}

	/**
	* Lukee massamuistilaitteelta osoitteesta operands[1], operands[2] datan rekisteriin operands[0]
	* @.pre operands.length() == 3
	*
	*/

	private void readStorage(byte[] operands) {
		byte co = (byte)(operands[1] + (byte)(operands[2] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		this.saveRegister(operands[0], storage.read(co));
	}

	/**
	* Tallentaa massamuistilaitteen osoitteeseen operands[1], operands[2] datan rekisteristä operands[0]
	* @.pre operands.length() == 3
	*
	*/

	private void saveStorage(byte[] operands) {
		byte co = (byte)(operands[1] + (byte)(operands[2] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		storage.save(co, this.readRegister(operands[0]));

	}


}
