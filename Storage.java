import java.io.*;

/*
* Jarko Papalitsas 501250
*
* Massamuistia kuvaava luokka. Tallentaa tiedot storage.bin-tiedostoon
*
*/

public class Storage {

	private byte[] storage; // Itse massamuistitaulukko
	private boolean changed; // Onko massamuistioperaatioita tehty tällä suorituskerralla (eli suoritetaanko tallennusoperaatiota fyysiselle levylle)

	/**
	* Alustusmetodi. Luetaan vanha tallenne taulukkoon
	*
	*/

	public Storage() {
		this.changed = false; // Massamuistiin ei olla koskettu, ellei true
		this.storage = new byte[256]; // 256-paikkainen massamuisti
		try {
			InputStream storageContainer = new FileInputStream("storage.bin"); // Avataan muistivedos fyysiseltä koneelta InputStreamiin
			storageContainer.read(this.storage); // Luetaan taulukkoon
			storageContainer.close(); // Suljetaan tiedosto
		}

		catch (IOException e) {
			Tools.debugMessage("Massamuistitiedostoa ei löydy!"); // Jos vanhaa massamuistia ei löydy heitetään debug-viesti, mutta jatketaan.
		}


	}

	/**
	* Lukee määrätystä osoitteesta dataa
	*
	* @.pre address 0=<address<this.storage.length()
	*/

	public byte read(byte address) {
		Tools.debugMessage("Luetaan massamuistilta osoitetta: " + address + ", arvo: " + this.storage[address]);
		return this.storage[address];

	}

	/**
	* Kirjoittaa määrättyyn osoitteeseen dataa
	*
	* @.pre address 0=<address<this.storage.length()
	*/

	public void save(byte address, byte data) {
		this.changed = true;
		Tools.debugMessage("Talletetaan massamuistin osoitteeseen: " + address + " arvo: " + this.storage[address]);
		this.storage[address] = data;

	}

	/**
	* Tallentaa virtuaalikoneistunnon aikana tehdyt muutokset fyysisen
	* kiintolevyn storage.bin-tiedsotoon.
	*
	*/

	public void sync() {
		if (this.changed == true) {
			try {
				OutputStream storageContainer = new FileOutputStream("storage.bin");
				storageContainer.write(this.storage);
			}
			catch (IOException e) {
				Tools.debugMessage("Massamuistitiedoston kirjoittamisessa tapahtui virhe!");

			}

		}

	}
}
