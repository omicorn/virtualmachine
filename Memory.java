import java.io.*;

/*
* Jarko Papalitsas 501250
*
* Muistia kuvaava luokka. 16x16 (256) kokoinen
*
*/


public class Memory {

	// Muistitaulukko
	private byte[][] memoryTable;

	/**
	* Muistin alustusfunktio. Asetetaan muistin osoiteavaruus ja
	* luetaan ohjelma muistiin. Muistivedos ladataan suoraan keskusmuistiin
	* massamuistin sijaan, koska virtuaalikoneessa ei ole biosia ohjaamassa
	* muistiin noutokäskyä massamuistin mbr:ltä.
	*/

	public Memory(String memoryDumpFile) throws IOException {
		this.memoryTable = new byte[16][16]; // Luodaan muistitaulukko
		InputStream memoryDump = new FileInputStream(memoryDumpFile); // Avataan muistivedos fyysiseltä koneelta InputStreamiin
		for (int i=0; this.memoryTable.length>i; i=i+1) {
			memoryDump.read(this.memoryTable[i]); // Luetaan koko muistivedos virtuaalikoneen virtuaalimuistiin	
		}
		memoryDump.close(); // Kun vedos on luettu muistiin, suljetaan tiedosto

	}

	/**
	* Lukee annetusta osoitteesta muistipaikan sisällön
	* @.pre 0 <= memoryAddress < this.memoryTable.length()
	*
	*/

	public byte readBlock(byte[] memoryAddress) {
		Tools.debugMessage("Luetaan muistiosoitetta: " + memoryAddress[0] + memoryAddress[1] + ", arvo: " + this.memoryTable[memoryAddress[0]][memoryAddress[1]]);
		return this.memoryTable[memoryAddress[0]][memoryAddress[1]];

	}


	/**
	* Tallentaa muistipaikkaan annetun datan (8 bittia, 1 tavu)
	* @.pre 0 <= memoryAddress < this.memoryTable.length()
	*
	*/

	public void storeBlock(byte[] memoryAddress, byte data) {
		Tools.debugMessage("Kirjoitetaan muistiosoitteeseen: " + memoryAddress[0] + memoryAddress[1] + ": " + data);
		this.memoryTable[memoryAddress[0]][memoryAddress[1]] = data;

	}




}
