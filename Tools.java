/**
* Jarko Papalitsas 501250
*
* Sisältää työkaluja ohjelman kehityksen helpottamiseksi
*
*/

public class Tools {


	static boolean debugMessages; // Näytetäänkö vietit tietokoneen toiminnasta vai ei

	/*
	* Funktio tulostaa viestit koneen toiminnasta, jos asetettu niin
	*
	*/

	public static void debugMessage(String message) {
		if (debugMessages == true) {
			System.out.println(message);

		}

	}

	/*
	* Asettaa viestien näkyvyysasetuksen
	*
	*/

	public static void showDebugMessages(boolean status) {
		debugMessages = status;

	}


}
