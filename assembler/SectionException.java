public class SectionException extends Exception {
	public SectionException() {
		super();

	}

	public SectionException(String message) {
		super(message);

	}

}
