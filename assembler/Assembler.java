import java.io.*;
import java.util.*;

/**
* Jarko Papalitsas 501250
*
* Yksinkertainen kääntäjä virtuaalikoneen testaamiseen. Syntaksi löytyy
* readme:stä.
*
*/

public class Assembler {

	private static ArrayList<Relation> sections = new ArrayList<Relation>(); // Sisältää Suhdeolioita, joissa yhdistetään osion nimi sen osoitteeseen
	private static byte[] bitCode; // Bittikoodi, josta tiedosto generoidaan
	private static int cursor = 0; // Tavutaulukkokirjoittajan tämänhetkinen sijainti
	private static String file; // Annettu lähdekooditiedosto

	public static void main(String[] args) throws IOException {
		if (args.length != 1) { // Tarkistetaan, että argumenttina on annettu jotakin
			System.out.println("VIRHE: Lähdekooditiedosto tarvitaan argumenttina");
			System.exit(1);

		}
		file = args[0]; // Asetetaan tiedostonimi omaan muuttujaansa
		bitCode = new byte[256];  // Muistivedoksen koko
		BufferedReader source = new BufferedReader(new FileReader(file)); // Luetaan lähdekoodi muistiin
		String line = source.readLine(); // Luetaan ensimmäinen rivi line-muuttujaan
		for (int i=0; line != null && bitCode.length > i; i=i+1) { // Aletaan käymään lähdetiedostoa rivi kerrallaan läpi
			try {
				System.out.print(i + ": "); // Tulostetaan rivinumero ja komento, jolloin käännön pysähdyttyä helpompi selvittää virheen paikka
				System.out.println(line);
				writeArray(mainParser(line)); // mainParser palauttaa alifunktioidensa tuloksen (byte-taulu), kun sille antaa rivin koodia. writeArray kirjottaa koodin tauluun binääritiedostoon kirjoittamista odottamaan
				line = source.readLine(); // Seuraava rivi
			}

			catch (SectionException e) { // Funktio lähettää SectionExceptoionin, koska ei haluta kirjata sitä lopuliseen koodiin
				line = source.readLine();
				continue;
			}

			catch (SectionNotFoundException e) { // Jos osiota ei ole (vielä) määritelty
				System.out.println(e);
				System.exit(1);

			}
			catch (UnknownCommandException e) { // Jos eteen tulee tuntematon komento
				System.out.println(e);
				System.exit(1);
			}

		}
		source.close(); // Kun koodi luettu taulukkoon binääreinä, suljetaan tiedosto
		writeBinaryFile(bitCode); // Kirjoitetaan taulukko tiedostoon
		System.exit(0); // Poistutaan onnistuneesti

	}

	/**
	* Kirjoittaa annetun byte-taulukon binääritiedostoksi.
	*
	*
	*/

	private static void writeBinaryFile(byte[] codeArray) throws IOException {
		OutputStream f = new FileOutputStream(file + ".bin");
		f.write(codeArray);
		f.close();

	}

	/**
	* Taulukkokirjoittaja, joka saa yhden konekäskyn byte-taulukkona.
	* Lisää siis yhden käskyn kaikkien ohjelmakäskyjen yhteistaulukkoon (byte bitCode)
	* ja pitää kirjaa, missä liikkuu cursor-muuttujan avulla.
	*
	*/

	private static void writeArray(byte[] instruction) {
		for (int i=0; instruction.length>i; i=i+1) { // 
			bitCode[cursor] = instruction[i];
			cursor = cursor + 1;
		}		

	}

	/**
	* Pääparseri. Parsii lähdekoodista pääkomennot ja kutsuu sen mukaan
	* oikeaa funktiota oikeantyyppiille komennoille.
	*
	* @.pre line != null
	*/

	private static byte[] mainParser(String line) throws SectionNotFoundException, SectionException, UnknownCommandException {
		String[] parsedCommand;
		parsedCommand = line.split(" ");
		if (parsedCommand[0].equals("SECTION")) {
			return section(parsedCommand[1]);
		}else if (parsedCommand[0].equals("IF")) {
			return jump(parsedCommand);
		}else if (parsedCommand[0].equals("LOAD")) {
			return operandParser((byte)1, parsedCommand[1]);
		}else if (parsedCommand[0].equals("LOADBYTE")) {
			return operandParser((byte)2, parsedCommand[1]);
		}else if (parsedCommand[0].equals("STORE")) {
			return operandParser((byte)3, parsedCommand[1]);
		}else if (parsedCommand[0].equals("MOVE")) {
			return operandParser((byte)4, parsedCommand[1]);
		}else if (parsedCommand[0].equals("ADD")) {
			return operandParser((byte)5, parsedCommand[1]);
		}else if (parsedCommand[0].equals("OR")) {
			return operandParser((byte)6, parsedCommand[1]);
		}else if (parsedCommand[0].equals("AND")) {
			return operandParser((byte)7, parsedCommand[1]);
		}else if (parsedCommand[0].equals("XOR")) {
			return operandParser((byte)8, parsedCommand[1]);
		}else if (parsedCommand[0].equals("ROTATE")) {
			return operandParser((byte)9, parsedCommand[1]);
		}else if (parsedCommand[0].equals("READSTRG")) {
			return operandParser((byte)11, parsedCommand[1]);
		}else if (parsedCommand[0].equals("SAVESTRG")) {
			return operandParser((byte)12, parsedCommand[1]);
		}else if (parsedCommand[0].equals("HALT")) {
			return operandParser((byte)0, parsedCommand[1]);
		}else {
			throw new UnknownCommandException("VIRHE: Virheellinen komento: " + parsedCommand[0]);

		}

	}

	/*
	* Parsii normaalifunktioista (ei if tai section) operandit ja muodostaa
	* konekäskyn niiden avulla. Konekäsky palautetaan kutsujalle, ja sitä
	* kautta päätyy lopulta kooditauluun.
	* opernads != null
	*/

	private static byte[] operandParser(byte command, String operands) {
		String[] operandArray;
		byte[] completeCommand = new byte[4];
		byte[] co = new byte[2];
		operandArray = operands.split(",");
		completeCommand[0] = command;
		for (int i=1; completeCommand.length>i; i=i+1) { // Lisätään operandit byteiksi muunnettuina bittikoodikirjoittajalle tarkoitettuun byte[]-taulukkoon
			completeCommand[i]=(byte)Integer.parseInt(operandArray[i-1]); // VÄÄRÄ MUOTO
		}

		co[0] = (byte)(completeCommand[1] + (byte)(completeCommand[0] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		co[1] = (byte)(completeCommand[3] + (byte)(completeCommand[2] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		return co;
		

	}

	/**
	* Osio-funktio. Lisää osiotaulukkoon tiedon nimestä ja osoitteesta.
	* IF-lauseissa viitatessa osion nimeen, tarkistetaan section-talukosta
	* oikea osioosoite.
	*
	* @.pre name != null
	*
	*/

	private static byte[] section(String name) throws SectionException {
		sections.add(new Relation(name, cursor));
		throw new SectionException();

	}

	/**
	* Muodostaa hyppykäskyn annetun SECTION-nimen perusteella, sekä muistiosoitteen
	*
	* parsedCommand.length() == 4;
	*/

	private static byte[] jump(String[] parsedCommand) throws SectionNotFoundException {
		byte[] completeCommand = new byte[4];
		byte[] co = new byte[2];
		completeCommand[0] = 10; // Hyppykäsky
		completeCommand[1] = (byte)Integer.parseInt(parsedCommand[1]);
		int sectionPos = 0;
		for (int i=0; sections.size()>i; i=i+1) { // Käydään läpi osiot
			if (sections.get(i).getName().equals(parsedCommand[3])) {
				sectionPos = sections.get(i).getAddress();
			}
			else {
				throw new SectionNotFoundException("VIRHE: viitattavaa osiota ei löyty: " + parsedCommand[3]);
			}
		}

		

		co[0] = (byte)(completeCommand[1] + (byte)(completeCommand[0] << 4)); // Yhdistetaan 4-bittiset operandit yhdeksi tavuksi
		co[1] = (byte) sectionPos;
		return co;

	}

}
