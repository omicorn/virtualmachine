public class SectionNotFoundException extends Exception {
	public SectionNotFoundException() {
		super();

	}

	public SectionNotFoundException(String message) {
		super(message);

	}

}
