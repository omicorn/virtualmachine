public class Relation {

	private final String name;
	private final int address;

	public Relation(String name, int address) {
		this.name = name;
		this.address = address;

	}

	public int getAddress() {
		return this.address;

	}

	public String getName() {
		return this.name;

	}

}
