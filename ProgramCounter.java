/**
* Jarko Papalitsas 501250
*
* Ohjelmalaskuriluokka. Antaa asettaa kohdistimen sekä lukea muistia "bittivirtana"
*
*/

public class ProgramCounter {
	private byte[] pos;

	/**
	* Alustusmetodi. Asetetaan aloituskohta
	*
	*/

	public ProgramCounter(byte[] pos) {
		this.pos = pos;

	}

	/**
	* Palauttaa ohjelmalaskurin arvon ja lisää sen arvoa yhdellä. Laskuri muodossa (0-F, 0-F)
	*
	*/

	public byte[] read() {
		byte[] current = new byte[2];
		current[0]=this.pos[0];
		current[1]=this.pos[1];
		if (this.pos[1]+1 >= 16) { 
			this.pos[1] = 0;
			this.pos[0] = (byte) (this.pos[0] + 1);
		}

		else {
			this.pos[1] = (byte) (this.pos[1] + 1);
		}
		return current;

	}

	/*
	* Uudelleenasettaa ohjelmalaskurin paikan
	*
	*/

	public void setPosition(byte[] position) {
		this.pos[0] = position[0];
		this.pos[1] = position[1];
	}


}
